// console.log("Hello World!")

let getCube = (number) => {
    let cube = number ** 3
    console.log(`The cube of ${number} is ${cube}`)
};

const address = ["258", "Washington Ave Nw" , "California" , "90011"];

const [houseNumber, street, state ,zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

const animal = {
    name: "Lolong",
    specie: "saltwater crocodile",
    weight: "1075 kgs",
    length: "20 ft 3 in",
};

console.log(`${animal.name} was a ${animal.specie}. He weighed at ${animal.weight} with a measurement of ${animal.length}.`)

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
    console.log(number);
});

let i = 0;
reducedArray = numbers.reduce((acc, cur) => {
        i++;
        acc;
        cur;
        return acc + cur;
    })
console.log(reducedArray);

class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund"
console.log(myDog);